package cc.iotkit.jql.executor;

import cc.iotkit.jql.ObjData;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapListExecutor implements IExecutor<ObjData, List<ObjData>> {

    public List<ObjData> executor(JdbcTemplate jdbcTemplate, String sql, Class<ObjData> elementCls, Map<String, Object> params) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        List<Map<String, Object>> maps = namedParameterJdbcTemplate.query(sql, params,
                new ColumnMapRowMapper()
        );

        List<ObjData> list = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            list.add(new ObjData(map));
        }
        return list;
    }

}
