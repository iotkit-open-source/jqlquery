package cc.iotkit.jql.executor;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Map;

public interface IExecutor<T, R> {
    R executor(JdbcTemplate jdbcTemplate, String sql, Class<T> elementCls, Map<String, Object> params);
}
