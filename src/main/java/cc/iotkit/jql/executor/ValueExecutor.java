package cc.iotkit.jql.executor;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Map;
import java.util.Optional;

public class ValueExecutor<T> implements IExecutor<T, T> {

    public T executor(JdbcTemplate jdbcTemplate, String sql, Class<T> elementCls, Map<String, Object> params) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        Map<String, Object> result = namedParameterJdbcTemplate.queryForMap(sql,
                params
        );
        Optional<Object> val = result.values().stream().findFirst();
        return (T) val.orElse(null);
    }

}
