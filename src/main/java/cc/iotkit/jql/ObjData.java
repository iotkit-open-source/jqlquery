package cc.iotkit.jql;

import cn.hutool.core.convert.Convert;

import java.util.HashMap;
import java.util.Map;

/**
 * 用于存储对象数据
 */
public class ObjData extends HashMap<String, Object> {

    public ObjData() {
        super();
    }

    public ObjData(Map<String, Object> map) {
        putAll(map);
    }

    public String getStr(String name) {
        return Convert.toStr(get(name));
    }

    public Integer getInt(String name) {
        return Convert.toInt(get(name));
    }

    public Long getLong(String name) {
        return Convert.toLong(get(name));
    }

    public void set(String name, Object val) {
        put(name, val);
    }

}
